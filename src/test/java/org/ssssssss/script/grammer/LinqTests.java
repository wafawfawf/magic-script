package org.ssssssss.script.grammer;

import org.junit.Test;
import org.ssssssss.script.BaseTest;

public class LinqTests extends BaseTest {

	@Test
	public void linq_1() {
		execute("grammar/linq/linq_1.ms");
	}

	@Test
	public void linq_2() {
		execute("grammar/linq/linq_2.ms");
	}

	@Test
	public void linq_3() {
		execute("grammar/linq/linq_3.ms");
	}

	@Test
	public void linq_4() {
		execute("grammar/linq/linq_4.ms");
	}

	@Test
	public void linq_5() {
		execute("grammar/linq/linq_5.ms");
	}

	@Test
	public void linq_6() {
		execute("grammar/linq/linq_6.ms");
	}
}
